# flickrsorter #

This utility allows you to sort photosets (albums) in flickr by description or by name.
Based on a idea from [this](http://weewillrockyou.blogspot.com/2009/07/sort-your-flickr-albums-in-ruby-with.html) blogpost that was made in Ruby.

### How to use? ###

* Clone source: 
```
$ git clone https://bitbucket.org/cyberplant/flickrsorter.git && cd flickrsorter
```
* Run it with -h to see command line info.

```
$ python sorter.py -h

usage: sorter.py [-h]
                 (--export EXPORT | --update UPDATE | --sortby {name,description})

Flicker Sorter.

optional arguments:
  -h, --help            show this help message and exit
  --export EXPORT       Exports album list to a file.
  --update UPDATE       Updates flickr with the contents of file.
  --sortby {name,description}
                        Sorts flicker albums by parameter.
```
* To sort your photosets by name:
 
```
$ python sorter.py --sortby name
```

* To sort by description:
```
$ python sorter.py --sortby description
```
* Also, you can export a file with the format <description> | <name> | <id> 
```
$ python sorter.py --export albums.txt
```
* Then you can rearrange the order of the photosets by hand, with a text editor o with a utility (like unix's sort for instance), and apply the changes with:
```
$ python sorter.py --update sorted_albums.txt
```