#!/usr/bin/env python

import flickrapi
import argparse


class Sorter:
    user_id = None

    def _debug(self, *s):
        print " ".join(s)

    def __init__(self, user_id=None):
        self.user_id = user_id

        api_key = 'ed73ac33b9577ca820c81f47e0c5f2f7'
        api_secret = 'e29b546659534370'

        self._debug("Initializing API...")
        flickr = flickrapi.FlickrAPI(api_key, api_secret, cache=True)

        (token, frob) = flickr.get_token_part_one(perms='write')
        if not token:
            raw_input("Press ENTER after you authorized this program")

        flickr.get_token_part_two((token, frob))
        self._debug("Initialized.")

        self.flickr = flickr

    def get_albums(self):
        self._debug("Getting photosets..")
        albums = self.flickr.photosets_getList()
        self._debug("Done.")

        return albums

    def update_order(self, order_str):
        self._debug("Setting photoset order..")
        result = self.flickr.photosets_orderSets(photoset_ids=order_str)
        self._debug("Done.")

        return result


def export(filename):
    albums = sorter.get_albums()
    albums_list = []
    biggest_namelength = 0
    biggest_desclength = 0
    for album in albums.find('photosets').findall('photoset'):
        album_name = album.find("title").text
        album_description = album.find("description").text
        item = {"id": album.attrib["id"],
                "description": album_description.encode("UTF-8"),
                "name": album_name.encode("UTF-8")}

        biggest_namelength = max(biggest_namelength, len(album_name))
        biggest_desclength = max(biggest_desclength, len(album_description))

        albums_list.append(item)

    with open(filename, "w") as f:
        for a in albums_list:
            name = a["name"].ljust(biggest_namelength)
            desc = a["description"].ljust(biggest_desclength)

            f.write("{description} | {name} | {id}\n".format(name=name,
                                                             description=desc,
                                                             id=a["id"]))


def update(filename):
    with open(filename, "r") as f:
        lines = f.readlines()

    order = []

    for l in lines:
        description, name, album_id = l.strip().split("|")
        print description, name, album_id
        order.append(album_id.strip())

    from xml.etree.ElementTree import dump
    dump(sorter.update_order(",".join(order)))


def sort_albums(order_by, reverse=True):
    albums = sorter.get_albums()

    items_by_desc = {}
    items_by_name = {}

    for album in albums.find('photosets').findall('photoset'):
        album_name = album.find("title").text
        album_description = album.find("description").text
        item = {"id": album.attrib["id"],
                "description": album_description.encode("UTF-8"),
                "name": album_name.encode("UTF-8")}

        items_by_desc[album_description] = item
        items_by_name[album_name] = item

    if order_by == "name":
        items = items_by_name
    else:
        items = items_by_desc

    albums_order = [items[i]["id"]
                    for i in sorted(items.keys(), reverse=reverse)]

    from xml.etree.ElementTree import dump
    dump(sorter.update_order(",".join(albums_order)))


if __name__ == "__main__":
    user_id = None

    parser = argparse.ArgumentParser(description='Flicker Sorter.')
    group = parser.add_mutually_exclusive_group(required=True)

    group.add_argument('--export', type=str,
                       default=argparse.SUPPRESS,
                       help='Exports album list to a file.')

    group.add_argument('--update', type=str,
                       default=argparse.SUPPRESS,
                       help='Updates flickr with the contents of file.')

    group.add_argument('--sortby', type=str,
                       default=argparse.SUPPRESS,
                       choices=["name", "description"],
                       help='Sorts flicker albums by parameter.')

    args = parser.parse_args()

    sorter = Sorter(user_id)

    if "export" in args:
        export(args.export)
    elif "update" in args:
        update(args.update)
    elif "sortby" in args:
        sort_albums(args.sortby)
